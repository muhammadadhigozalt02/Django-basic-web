from django.shortcuts import render, redirect 
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout

# Create your views here.
def register_view(request):
    if request.method == "POST": 
        form = UserCreationForm(request.POST) 
        if form.is_valid(): 
            login(request, form.save()) 
            return redirect("posts:list")
    else:
        form = UserCreationForm()
    return render(request, "users/register.html", { "form": form })

# ini logic nya if request post habis itu muncul form,
# habis itu isi form, terus di cek kalau valid dalam arti tidak ada username yang sama atau pass salah maka di save, 
# dan akan kembali ke list post. else berati gada data yang di isi, tapi tetap ada form


def login_view(request):
    if request.method == "POST": 
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            # LOGIN HERE
            login(request, form.get_user())
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                return redirect("posts:list")
    else:
        form = AuthenticationForm()
    return render(request, "users/login.html", { "form": form })


def logout_view(request):
    if request.method == "POST": 
        logout(request)
        return redirect("posts:list")
    else:
        form = AuthenticationForm()
    return render(request, "users/logout.html", { "form": form })
