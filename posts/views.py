from django.shortcuts import redirect, render
from.models import Post
from django.contrib.auth.decorators import login_required
from . import forms
#from django.http import HttpResponse
# Create your views here.

def posts_list(request):
    #posts = Post.objects.all().order_by('-date') #minus buat urutan terakkhir
    posts = Post.objects.all()
    return render(request, 'posts/posts_list.html', {'posts' :
    posts})

def post_page(request, slug):
    #return HttpResponse(slug)
    post = Post.objects.get(slug=slug)
    return render(request, 'posts/post_page.html', {'post' :
    post})

def post_new(request):
    return render(request, 'posts/post_new.hmtl')

# logic untuk cek kalo user udah login atau belum, kalau udah bisa post_new
@login_required(login_url="/users/login/")
def post_new(request):
    if request.method == 'POST':
        form = forms.CreatePost(request.POST, request.FILES)
        if form.is_valid():
            # save with user
            newpost = form.save(commit=False)
            newpost.author = request.user
            newpost.save()
            return redirect('posts:list')
    else:
        form = forms.CreatePost()
    return render(request, 'posts/post_new.html', {'form': form})