from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Post(models.Model):
    title = models.CharField(max_length=75)
    body = models.TextField()
    slug = models.SlugField()
    date = models.DateTimeField(auto_now_add=True)
    banner = models.ImageField(default='fallback.png', blank=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, default=None) 
        # karena dlama database butuh foreign key
        # on_delete dibutuhkan karena untuk memberi tahu database bagaimana meng-handle data
        # jika relationship (on_delete) nya dihapus.
        # CASCADE berati "Ok jika user dihapus maka hapus semua post nya juga."
        # Sehingga tidak ada "broken-relationship"
    def __str__(self):
        return self.title
    